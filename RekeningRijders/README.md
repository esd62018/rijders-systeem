# Rekening rijders systeem #

### Setup project environment ###
Disclaimer: The following steps are described to setup the environment for IntelliJ. Other editor users are on there own.


### Requirements ###
- IntelliJ 2017.3.4 Ultimate
- GlassFish 5.0.0 server
- MySQL database

### Steps ###
1. Download or clone repository
2. Open in intelliJ
3. When prompt enable maven auto import
4. Configure GlassFish (see https://www.jetbrains.com/help/idea/creating-and-running-your-first-restful-web-service-on-glassfish-application-server.html)
5. Configure database
    * Make sure to have a database schema available in MySQL
    * Create a JDBC connection pool and resource in the GlassFish admin environment to connect with your database (see https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-glassfish-config.html)
    * Modify both the persistence.xml and the glassfish-resource.xml files matching your database and connection pool
    * Make sure you don't forget the test persistence.xml one in case you like to run some tests ^^
6. Configure Arquillian (see https://www.jetbrains.com/help/idea/arquillian-a-quick-start-guide.html)
    * Just follow the steps to either setup an embedded container or an managed container
    * Follow step 5 to configure a database environment which can be used by Arquillian
7. Run application :) 


### Dependencies ###
Project uses both maven dependencies which should be fetched automatically as well as libraries who are located within the project itself (/lib directory).
