package domain;

import javax.persistence.*;

/**
 * Vehicle entity contains simple vehicle details
 */
@Entity
@Table(name = "VEHICLE")
@NamedQueries({
  @NamedQuery(
    name = "Vehicle.findAll",
    query = "SELECT v FROM Vehicle v"
  ),
  @NamedQuery(
    name = "Vehicle.findByLicensePlate",
    query = "SELECT v FROM Vehicle v WHERE v.licensePlate = :licensePlate"
  ),
  @NamedQuery(
    name = "Vehicle.findByCarTracker",
    query = "SELECT v FROM Vehicle v WHERE v.carTracker = :carTracker"
  )
})
public class Vehicle {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  @Column(name = "BRAND")
  private String brand;
  @Column(name = "MODEL")
  private String model;
  @Column(name = "LICENSE_PLATE", unique = true)
  private String licensePlate;
  @Column(name = "BUILD_YEAR")
  private String buildYear;
  @Column(name = "COLOR")
  private String color;
  @Column(name = "CAR_TRACKER", unique = true)
  private String carTracker;
  @Column(name = "RATE_CATEGORY")
  private String rateCategory;

  public Vehicle() {

  }

  public Vehicle(Long id) {
    this.id = id;
  }

  public Vehicle(String brand, String model, String licensePlate, String buildYear, String color, String carTracker) {
    this.brand = brand;
    this.model = model;
    this.licensePlate = licensePlate;
    this.buildYear = buildYear;
    this.color = color;
    this.carTracker = carTracker;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getBuildYear() {
    return buildYear;
  }

  public void setBuildYear(String buildYear) {
    this.buildYear = buildYear;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getCarTracker() {
    return carTracker;
  }

  public void setCarTracker(String carTracker) {
    this.carTracker = carTracker;
  }

  public String getRateCategory() {
    return rateCategory;
  }

  public void setRateCategory(String rateCategory) {
    this.rateCategory = rateCategory;
  }

  @Override
  public String toString() {
    return "id: " + id
      + " brand: " + brand
      + " model: " + model
      + " license place: " + licensePlate
      + " build year: " + buildYear
      + " color: " + color
      + " carTracker: " + carTracker;
  }
}
