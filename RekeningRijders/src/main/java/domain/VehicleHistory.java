package domain;

import javax.persistence.*;

/**
 * Vehicle history entity containing references to owner and vehicle entities,
 * and has a from and till date to keep track of the owners and their vehicles.
 *
 * A vehicle history entity always contains one owner and one vehicle
 *
 * @see Owner
 * @see Vehicle
 */
@Entity
@Table(name = "VEHICLE_HISTORY")
@NamedQueries({
  @NamedQuery(
    name = "VehicleHistory.findAll",
    query = "SELECT vh from VehicleHistory vh"
  ),
  @NamedQuery(
    name = "VehicleHistory.findAllFromOwner",
    query = "SELECT vh from VehicleHistory vh WHERE vh.owner.id = :ownerId"
  )
})
public class VehicleHistory {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  @Column(name = "FROM_DATE")
  private String fromDate;
  @Column(name = "TILL_DATE")
  private String tillDate;
  @ManyToOne
  @JoinColumn(name = "OWNER_ID")
  private Owner owner;
  @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
  @JoinColumn(name = "VEHICLE_ID")
  private Vehicle vehicle;

  public VehicleHistory() {

  }

  public VehicleHistory(Long id) {
    this.id = id;
  }

  public VehicleHistory(String fromDate, String tillDate) {
    this.fromDate = fromDate;
    this.tillDate = tillDate;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFromDate() {
    return fromDate;
  }

  public void setFromDate(String from) {
    this.fromDate = from;
  }

  public String getTillDate() {
    return tillDate;
  }

  public void setTillDate(String till) {
    this.tillDate = till;
  }

  public Owner getOwner() {
    return owner;
  }

  public void setOwner(Owner owner) {
    this.owner = owner;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  @Override
  public String toString() {
    return "id: " + id
      + " from date: " + fromDate
      + " till date: " + tillDate
      + " vehicle: " + vehicle.toString()
      + " owner: " + owner.toString();
  }
}
