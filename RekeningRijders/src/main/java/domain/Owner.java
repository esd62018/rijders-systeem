package domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Owner entity containing basic personal details
 *
 * An owner entity can have multiple vehicle histories
 * @see VehicleHistory
 */
@Entity
@Table(name = "OWNER")
@NamedQueries({
  @NamedQuery(
    name = "Owner.findAll",
    query = "SELECT o FROM Owner o"
  ),
  @NamedQuery(
    name = "Owner.findByLicensePlate",
    query =
      "SELECT o " +
        "FROM Owner o, VehicleHistory vh, Vehicle v " +
        "WHERE o.id = vh.owner.id " +
        "AND v.id = vh.vehicle.id " +
        "AND v.licensePlate = :licensePlate " +
        "AND vh.tillDate IS NULL"
  ),
  @NamedQuery(
    name = "Owner.findByCarTracker",
    query =
      "SELECT o " +
      "FROM Owner o, VehicleHistory vh, Vehicle v " +
      "WHERE o.id = vh.owner.id " +
      "AND vh.vehicle.id = v.id " +
      "AND v.carTracker = :carTracker " +
      "AND vh.tillDate IS NULL"
  )
})
public class Owner {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(unique = true, name = "CITIZEN_SERVICE_NUMBER")
  private String citizenServiceNumber;
  @Column(name = "STREET")
  private String street;
  @Column(name = "HOUSE_NUMBER")
  private String houseNumber;
  @Column(name = "ZIP_CODE")
  private String zipCode;
  @Column(name = "CITY")
  private String city;
  @Column(name = "COUNTRY")
  private String country;
  @Column(name = "FIRST_NAME")
  private String firstName;
  @Column(name = "LAST_NAME")
  private String lastName;
  @OneToMany(mappedBy = "owner")
  private List<VehicleHistory> vehicleHistories = new ArrayList<>();

  public Owner() {

  }

  public Owner(Long id) {
    this.id = id;
  }

  public Owner(String citizenServiceNumber, String street, String houseNumber, String zipCode, String city, String country, String firstName, String lastName) {
    this.citizenServiceNumber = citizenServiceNumber;
    this.street = street;
    this.houseNumber = houseNumber;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCitizenServiceNumber() {
    return citizenServiceNumber;
  }

  public void setCitizenServiceNumber(String citizenServiceNr) {
    this.citizenServiceNumber = citizenServiceNr;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String address) {
    this.street = address;
  }

  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public List<VehicleHistory> getVehicleHistories() {
    return vehicleHistories;
  }

  public void setVehicleHistories(List<VehicleHistory> vehicleHistories) {
    this.vehicleHistories = vehicleHistories;
  }

  public void addVehicleHistory(VehicleHistory vehicleHistory) {
    vehicleHistories.add(vehicleHistory);
  }

  @Override
  public String toString() {
    return "id: " + id
      + " first name: " + firstName
      + " last name: " + lastName;
  }
}
