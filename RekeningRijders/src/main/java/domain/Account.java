package domain;

import javax.persistence.*;

/**
 * Account entity containing login credentials
 *
 * An account entity always belongs to a single owner entity
 * @see Owner
 */
@Entity
@Table(name = "ACCOUNT")
@NamedQueries({
  @NamedQuery(
    name = "Account.auth",
    query = "SELECT u FROM Account u WHERE u.username = :username AND u.password = :password"
  )
})
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  @Column(unique = true, name = "USERNAME")
  private String username;
  @Column(name = "PASSWORD")
  private String password;
  @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
  @JoinColumn(name = "OWNER_ID")
  private Owner details;

  public Account() {

  }

  public Account(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public Account(Long id, String username, String password) {
    this.id = id;
    this.username = username;
    this.password = password;
  }

  public Account(String username, String password, Owner details) {
    this.username = username;
    this.password = password;
    this.details = details;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Owner getDetails() {
    return details;
  }

  public void setDetails(Owner details) {
    this.details = details;
  }

  @Override
  public String toString() {
    return "id: " + id
      + " username: " + username
      + " password: " + password;
  }
}
