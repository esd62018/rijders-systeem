package service;

import dao.VehicleDao;
import domain.Vehicle;
import dto.VehicleDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class VehicleService {

  @Inject
  private VehicleDao vehicleDao;

  public VehicleService() {

  }

  public List<Vehicle> findAll() {
    return vehicleDao.findAll();
  }

  public Vehicle findByLicensePlate(String licensePlate) {
    return vehicleDao.findByLicensePlate(licensePlate);
  }

  public Vehicle findByCarTracker(String carTracker) {
    return vehicleDao.findByCarTracker(carTracker);
  }

}
