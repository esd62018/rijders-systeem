package service;

import dao.OwnerDao;
import domain.Owner;

import javax.ejb.Stateless;
import javax.inject.Inject;

import java.util.List;

@Stateless
public class OwnerService {

  @Inject
  private OwnerDao ownerDao;

  public OwnerService() {

  }

  // TODO:
  public List<Owner> findAll() {
    return ownerDao.findAll();
  }

  // TODO:
  public Owner findById(Long id) {
    return ownerDao.findById(id);
  }

  // TODO:
  public Owner findByLicensePlate(String licensePlate) {
    return ownerDao.findByLicensePlate(licensePlate);
  }

  public Owner findByCarTracker(String carTracker) {
    return ownerDao.findByCarTracker(carTracker);
  }

}
