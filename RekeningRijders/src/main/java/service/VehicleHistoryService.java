package service;

import dao.VehicleHistoryDao;
import domain.VehicleHistory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class VehicleHistoryService {

  @Inject
  private VehicleHistoryDao vehicleHistoryDao;

  public VehicleHistoryService() {

  }

  public VehicleHistory findVehicleHistoryById(Long id) {
    return vehicleHistoryDao.findVehicleHistoryById(id);
  }

  public List<VehicleHistory> findAllVehicleHistories() {
    return vehicleHistoryDao.findAllVehicleHistories();
  }

  public List<VehicleHistory> findVehicleHistoryFromOwner(Long ownerId) {
    return vehicleHistoryDao.findVehicleHistoryFromOwner(ownerId);
  }

  public VehicleHistory create(VehicleHistory vehicleHistory) {
    return vehicleHistoryDao.create(vehicleHistory);
  }

  public VehicleHistory update(VehicleHistory vehicleHistory) {
    return vehicleHistoryDao.update(vehicleHistory);
  }

  public void delete(Long id) {
    vehicleHistoryDao.delete(id);
  }
}
