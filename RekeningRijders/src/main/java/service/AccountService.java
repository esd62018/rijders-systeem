package service;

import dao.AccountDao;
import domain.Account;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AccountService {

  @Inject
  private AccountDao accountDao;

  public AccountService() {

  }

  public Account authenticate(Account account) {
    return accountDao.findAccountByLoginCredential(account.getUsername(), account.getPassword());
  }

  public Account signUp(Account account) {
    return accountDao.create(account);
  }

  public Account update(Account account) {
    return accountDao.update(account);
  }

  public void delete(Long id) {
    accountDao.delete(id);
  }
}
