package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
  private static Config instance = null;
  private Properties properties;

  private Config () {
    ClassLoader classLoader = getClass().getClassLoader();
    InputStream input = classLoader.getResourceAsStream("config/config.properties");

    this.properties = new Properties();

    try {
      properties.load(input);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Config getInstance() {
    if (instance == null) {
      instance = new Config();
    }

    return instance;
  }

  public Properties getProperties() {
    return properties;
  }
}
