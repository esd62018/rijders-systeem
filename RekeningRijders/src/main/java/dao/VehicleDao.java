package dao;

import domain.Vehicle;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class VehicleDao {

  @PersistenceContext
  private EntityManager entityManager;

  public VehicleDao() {

  }

  public List<Vehicle> findAll() {
    return entityManager
      .createNamedQuery("Vehicle.findAll", Vehicle.class)
      .getResultList();
  }

  public Vehicle findByLicensePlate(String licensePlate) {
    return entityManager
      .createNamedQuery("Vehicle.findByLicensePlate", Vehicle.class)
      .setParameter("licensePlate", licensePlate)
      .getSingleResult();
  }

  public Vehicle findByCarTracker(String carTracker) {
    return entityManager
      .createNamedQuery("Vehicle.findByCarTracker", Vehicle.class)
      .setParameter("carTracker", carTracker)
      .getSingleResult();
  }
}
