package dao;

import domain.Account;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AccountDao {

  @PersistenceContext
  private EntityManager entityManager;

  public AccountDao () {

  }

  public Account findAccountByLoginCredential(String username, String password) {
    return entityManager
      .createNamedQuery("Account.auth", Account.class)
      .setParameter("username", username)
      .setParameter("password", password)
      .getSingleResult();

    // TODO: check HQL | criteria API example
  }

  public Account create(Account account) {
    entityManager.persist(account);

    return account;
  }

  public Account update(Account account) {
    entityManager.merge(account);

    return account;
  }

  public void delete(Long id) {
    entityManager.remove(entityManager.find(Account.class, id));
  }
}
