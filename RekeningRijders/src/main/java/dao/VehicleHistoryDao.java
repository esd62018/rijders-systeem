package dao;

import domain.VehicleHistory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

@Stateless
public class VehicleHistoryDao {

  @PersistenceContext
  private EntityManager entityManager;

  public VehicleHistoryDao() {

  }

  public VehicleHistory findVehicleHistoryById(Long id) {
    return entityManager.find(VehicleHistory.class, id);
  }

  public List<VehicleHistory> findAllVehicleHistories() {
    return entityManager
      .createNamedQuery("VehicleHistory.findAll", VehicleHistory.class)
      .getResultList();
  }

  public List<VehicleHistory> findVehicleHistoryFromOwner(Long ownerId) {
    return entityManager
      .createNamedQuery("VehicleHistory.findAllFromOwner", VehicleHistory.class)
      .setParameter("ownerId", ownerId)
      .getResultList();
  }

  public VehicleHistory create(VehicleHistory vehicleHistory) {
    entityManager.persist(vehicleHistory);

    return vehicleHistory;
  }

  public VehicleHistory update(VehicleHistory vehicleHistory) {
    entityManager.merge(vehicleHistory);

    return vehicleHistory;
  }

  public void delete(Long id) {
    entityManager.remove(entityManager.find(VehicleHistory.class, id));
  }
}
