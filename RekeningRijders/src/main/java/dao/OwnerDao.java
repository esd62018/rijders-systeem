package dao;

import domain.Owner;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

@Stateless
public class OwnerDao {
  @PersistenceContext
  private EntityManager entityManager;

  public OwnerDao() {

  }

  public List<Owner> findAll() {
    return entityManager
      .createNamedQuery("Owner.findAll", Owner.class)
      .getResultList();
  }

  public Owner findById(Long id) {
    return entityManager.find(Owner.class, id);
  }

  public Owner findByLicensePlate(String licensePlate) {
    return entityManager
      .createNamedQuery("Owner.findByLicensePlate", Owner.class)
      .setParameter("licensePlate", licensePlate)
      .getSingleResult();
  }

  public Owner findByCarTracker(String carTracker) {
    return entityManager
      .createNamedQuery("Owner.findByCarTracker", Owner.class)
      .setParameter("carTracker", carTracker)
      .getSingleResult();
  }
}
