package rest;

import domain.VehicleHistory;
import dto.VehicleHistoryDto;
import service.VehicleHistoryService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/vehicle-histories")
public class VehicleHistoryRest {

  @Inject
  private VehicleHistoryService vehicleHistoryService;

  /**
   * Route to get all vehicle histories
   *
   * @return           response including a list of vehicle histories in JSON format
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAll() {
    List<VehicleHistoryDto> vehicleHistoryDtos =  null;
    int status = 200;

    try {
      vehicleHistoryDtos = vehicleHistoryService
        .findAllVehicleHistories()
        .stream()
        .map(VehicleHistoryDto::fromVehicleHistory).collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicleHistoryDtos)
      .build();
  }


  /**
   * Route to get all vehicle histories from an specific owner
   *
   * @param ownerId    the owner id to fetch all vehicle histories from
   * @return           response including a list of vehicle histories in JSON format
   */
  @GET
  @Path("/{ownerId}")
  @Produces({ APPLICATION_JSON })
  public Response getVehicleFromAccount(@PathParam("ownerId") Long ownerId) {
    List<VehicleHistoryDto> vehicleHistoryDtos =  null;
    int status = 200;

    try {
      vehicleHistoryDtos = vehicleHistoryService
        .findVehicleHistoryFromOwner(ownerId)
        .stream()
        .map(VehicleHistoryDto::fromVehicleHistory).collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicleHistoryDtos)
      .build();
  }

  /**
   * Route to create a new vehicle history
   *
   * Required request body JSON format:
   *  {
   *    "fromDate": "2018-01-13 18:00:00",
   *    "tillDate": "2018-01-15 18:00:00",
   *    "owner": {
   *      "id": ownerId
   *    },
   *    "vehicle": {
   *      "brand": "new",
   *      "model": "new",
   *      "buildYear": "new",
   *      "color": "new",
   *      "licensePlate": "unique",
   *      "carTracker": "unique"
   *    }
   *  }
   *
   * The owner id and a vehicle is required!
   *
   * Restrictions: owner needs to exist and license plate need to be unique
   *
   * @param vehicleHistory     vehicle history instance serialized from request body
   * @return                   response including a vehicle history instance into JSON format when successfully saved in the db
   */
  @POST
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response create(VehicleHistory vehicleHistory) {
    VehicleHistoryDto vehicleHistoryDto = null;
    int status = 200;

    try {
      vehicleHistoryDto = VehicleHistoryDto.fromVehicleHistory(vehicleHistoryService.create(vehicleHistory));
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicleHistoryDto)
      .build();
  }

  /**
   * Route to update an existing vehicle history
   *
   * Required request body JSON format:
   *  {
   *    "id": vehicleHistoryId,
   *    "fromDate": "updated",
   *    "tillDate": "updated",
   *    "owner": {
   *      "id": ownerId
   *    },
   *    "vehicle": {
   *      "id": vehicleId
   *    }
   *  }
   *
   * Only the id fields are required, all other fields get updated
   * Note: field with the value of null get updated as well so make sure to include values who don't need to get updated
   *
   * @param vehicleHistory     vehicle history instance serialized from request body
   * @return                   response including a vehicle history instance into JSON format when successfully saved in the db
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(VehicleHistory vehicleHistory) {
    VehicleHistoryDto vehicleHistoryDto = null;
    int status = 200;

    try {
      vehicleHistoryDto = VehicleHistoryDto.fromVehicleHistory(vehicleHistoryService.update(vehicleHistory));
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicleHistoryDto)
      .build();
  }

  /**
   * Route to delete existing vehicle history
   *
   * Will return successful response without content (204) on success
   *
   * @param id    the id of the vehicle history who needs to be deleted from the db
   * @return      response status 200 (ok) on success
   */
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    int status = 200;

    try {
      vehicleHistoryService.delete(id);
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response.status(status).build();
  }
}
