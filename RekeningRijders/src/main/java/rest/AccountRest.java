package rest;

import domain.Account;
import dto.AccountDto;
import service.AccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/accounts")
public class AccountRest {

  @Inject
  private AccountService accountService;

  /**
   * Route update an existing user's data
   *
   * Required request body JSON format:
   *  {
   *    "id": accountId,
   *    "password": "updated",
   *    "username": "updated",
   *    "details": {
   *      "id": ownerId,
   *      "citizenServiceNumber": "1234",
   *      "city": "updated",
   *      "country": "updated",
   *      "zipCode": "updated",
   *      "street": "updated",
   *      "houseNumber": "updated",
   *      "firstName": "updated",
   *      "lastName": "updated"
   *    }
   *  }
   *
   * Only the id fields are required, all other field will get updated
   *
   * Note: field with the value of null get updated as well so make sure to include values who don't need to get updated
   *
   * Restrictions: username and citizenServiceNumber needs to be unique!!!
   *
   * @param account   account instance serialized from request body
   * @return          response containing an account in JSON format on success
   */
  @PUT
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response update(Account account) {
    AccountDto accountDto = null;
    int status = 200;

    try {
      accountDto = AccountDto.fromAccount(accountService.update(account));
    } catch (Exception e) {
      // TODO: handle specific errors
      status = 500;
    }

    return Response
      .status(status)
      .entity(accountDto)
      .build();
  }

  /**
   * Route to delete existing user
   *
   * Will return successful response without content (204) on success
   *
   * @param id    the id of the user who needs to be deleted from the db
   * @return      response status 200 (ok) on success
   */
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") Long id) {
    int status = 200;

    try {
      accountService.delete(id);
    } catch (Exception e) {
      // TODO: handle different exceptions
      status = 500;
    }

    return Response.status(status).build();
  }
}
