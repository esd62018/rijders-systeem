package rest;

import dto.RatePerKilometerDto;
import utils.Config;
import utils.Constants;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/rates")
public class RatePerKilometerRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");
  private Client client;

  public RatePerKilometerRest () {
    client = ClientBuilder.newClient();
  }

  /**
   * Route to get all ratesPerKilometer.
   *
   * @return  a response containing a list of ratesPerKilometer in JSON format on success
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getRatesPerKilometer() {
    List<RatePerKilometerDto> rates = null;
    int status = 200;

    try {
      rates = client.target(Constants.GOVERNMENT_API_URL)
        .path("rates")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get(new GenericType<List<RatePerKilometerDto>>(){});
    } catch (Exception e) {
      e.printStackTrace();

      status = 500;
    }

    return Response
      .status(status)
      .entity(rates)
      .build();
  }
}
