package rest;

import domain.Account;
import dto.AccountDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import service.AccountService;
import utils.Constants;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/auth")
public class AuthRest {
  private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
  private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

  @Inject
  private AccountService accountService;

  /**
   * Route to authenticated user with login credentials.
   *
   * Required request body JSON format:
   *  {
   *    "username": "username",
   *    "password": "password"
   *  }
   *
   * All above fields are required!
   *
   * @param account   account instance serialized from request body
   * @return          response containing an account in JSON format and a JWT
   */
  @POST
  @Path("/login")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response authenticate(Account account) {
    ResponseBody responseBody = new ResponseBody();
    int status = 200;

    try {
      AccountDto accountDto = AccountDto.fromAccount(accountService.authenticate(account));
      LocalDateTime localDateTime = new Date()
        .toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
        .plusDays(3);

      Date expiration = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

      String token = Jwts.builder()
        .setExpiration(expiration)
        .setSubject(String.valueOf(accountDto.getId()))
        .signWith(SignatureAlgorithm.HS512, Constants.JWS_SECRET)
        .compact();

      responseBody.setAuth(accountDto);
      responseBody.setExpireDate(dateFormat.format(expiration));
      responseBody.setToken(token);

    } catch (Exception e) {
      if (e instanceof EJBTransactionRolledbackException) {   // Caused by no account found
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(responseBody)
      .build();
  }


  /**
   * Route to sign up a new user
   *
   * Required request body JSON format:
   *  {
   *    "username": "username",
   *    "password": "password",
   *    "details": {
   *      "citizenServiceNumber": "unique",
   *      "city": "new",
   *      "country": "new",
   *      "zipCode": "new",
   *      "street": "new",
   *      "houseNumber": "new",
   *      "firstName": "new",
   *      "lastName": "new"
   *    }
   *  }
   *
   * Username and password are required!
   *
   * Details are optional, those can be set later on with update as well
   *
   * Restrictions: username and citizenServiceNumber needs to be unique!!!
   *
   * @param account   account instance serialized from request body
   * @return          response containing an account in JSON format on success
   */
  @POST
  @Path("/sign-up")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response signUp(Account account) {
    AccountDto accountDto = null;
    int status = 200;

    try {
      accountDto = AccountDto.fromAccount(accountService.signUp(account));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(accountDto)
      .build();
  }

  public class ResponseBody {
    private AccountDto auth;
    private String token;
    private String expireDate;

    public ResponseBody() {

    }

    public AccountDto getAuth() {
      return auth;
    }

    public void setAuth(AccountDto auth) {
      this.auth = auth;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public String getExpireDate() {
      return expireDate;
    }

    public void setExpireDate(String expireDate) {
      this.expireDate = expireDate;
    }
  }
}
