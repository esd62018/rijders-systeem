package rest;

import com.google.gson.Gson;
import dto.InvoiceDto;
import utils.Config;
import utils.Constants;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/invoices")
public class InvoiceRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");
  private Client client;

  public InvoiceRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Path("/{userId}")
  @Produces({ APPLICATION_JSON })
  public Response getInvoices(@PathParam("userId") Long userId) {
    List<InvoiceDto> invoices = null;
    int status = 200;

    try {
      invoices = client.target(Constants.GOVERNMENT_API_URL)
        .path("invoices")
        .path(String.valueOf(userId))
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get(new GenericType<List<InvoiceDto>>(){});

      if (invoices.size() == 0) {
        // Tmp: fake invoices data
        invoices = Arrays.asList(new Gson().fromJson(INVOICE_JSON, InvoiceDto[].class));
      }

    } catch (Exception e) {
      e.printStackTrace();

//       status = 500;
      // Tmp: fake invoices data
      invoices = Arrays.asList(new Gson().fromJson(INVOICE_JSON, InvoiceDto[].class));
    }

    return Response
      .status(status)
      .entity(invoices)
      .build();
  }

  private static final String INVOICE_JSON = "[\n" +
    "    {\n" +
    "        \"carTracker\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "        \"category\": \"GASOLINE\",\n" +
    "        \"id\": 1,\n" +
    "        \"invoiceDate\": \"01-06-2018\",\n" +
    "        \"journeys\": [\n" +
    "            {\n" +
    "                \"endDate\": \"2018-05-08T10:22:31:000Z\",\n" +
    "                \"id\": \"1\",\n" +
    "                \"serialNumber\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "                \"transLocations\": [\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:20:00Z[UTC]\",\n" +
    "                        \"distance\": 0,\n" +
    "                        \"lat\": 51.064236,\n" +
    "                        \"lon\": 3.553117,\n" +
    "                        \"price\": 0,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:21:00Z[UTC]\",\n" +
    "                        \"distance\": 1.1119712195800118,\n" +
    "                        \"lat\": 51.074236,\n" +
    "                        \"lon\": 3.553217,\n" +
    "                        \"previousLat\": 51.064236,\n" +
    "                        \"previousLon\": 3.553117,\n" +
    "                        \"price\": 0.06671827317480071,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:22:00Z[UTC]\",\n" +
    "                        \"distance\": 1.112037038441509,\n" +
    "                        \"lat\": 51.084236,\n" +
    "                        \"lon\": 3.553417,\n" +
    "                        \"previousLat\": 51.074236,\n" +
    "                        \"previousLon\": 3.553217,\n" +
    "                        \"price\": 0.06672222230649054,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    }\n" +
    "                ]\n" +
    "            },\n" +
    "            {\n" +
    "                \"endDate\": \"2018-05-12T10:22:31:000Z\",\n" +
    "                \"id\": \"2\",\n" +
    "                \"serialNumber\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "                \"transLocations\": [\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:20:00Z[UTC]\",\n" +
    "                        \"distance\": 0,\n" +
    "                        \"lat\": 50.358211,\n" +
    "                        \"lon\": 5.275686,\n" +
    "                        \"price\": 0,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:21:00Z[UTC]\",\n" +
    "                        \"distance\": 0.13189693148428366,\n" +
    "                        \"lat\": 50.359211,\n" +
    "                        \"lon\": 5.274686,\n" +
    "                        \"previousLat\": 50.358211,\n" +
    "                        \"previousLon\": 5.275686,\n" +
    "                        \"price\": 0.006594846574214184,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:22:00Z[UTC]\",\n" +
    "                        \"distance\": 0.13189612769850817,\n" +
    "                        \"lat\": 50.360211,\n" +
    "                        \"lon\": 5.273686,\n" +
    "                        \"previousLat\": 50.359211,\n" +
    "                        \"previousLon\": 5.274686,\n" +
    "                        \"price\": 0.006594806384925409,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    }\n" +
    "                ]\n" +
    "            }\n" +
    "        ],\n" +
    "        \"ownerId\": 1,\n" +
    "        \"paymentStatus\": \"OPEN\",\n" +
    "        \"totalAmount\": 0.15\n" +
    "    },\n" +
    "    {\n" +
    "        \"carTracker\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "        \"category\": \"GASOLINE\",\n" +
    "        \"id\": 21,\n" +
    "        \"invoiceDate\": \"01-06-2018\",\n" +
    "        \"journeys\": [\n" +
    "            {\n" +
    "                \"endDate\": \"2018-05-08T10:22:31:000Z\",\n" +
    "                \"id\": \"1\",\n" +
    "                \"serialNumber\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "                \"transLocations\": [\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:20:00Z[UTC]\",\n" +
    "                        \"distance\": 0,\n" +
    "                        \"lat\": 51.064236,\n" +
    "                        \"lon\": 3.553117,\n" +
    "                        \"price\": 0,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:21:00Z[UTC]\",\n" +
    "                        \"distance\": 1.1119712195800118,\n" +
    "                        \"lat\": 51.074236,\n" +
    "                        \"lon\": 3.553217,\n" +
    "                        \"previousLat\": 51.064236,\n" +
    "                        \"previousLon\": 3.553117,\n" +
    "                        \"price\": 0.06671827317480071,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-08T10:22:00Z[UTC]\",\n" +
    "                        \"distance\": 1.112037038441509,\n" +
    "                        \"lat\": 51.084236,\n" +
    "                        \"lon\": 3.553417,\n" +
    "                        \"previousLat\": 51.074236,\n" +
    "                        \"previousLon\": 3.553217,\n" +
    "                        \"price\": 0.06672222230649054,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    }\n" +
    "                ]\n" +
    "            },\n" +
    "            {\n" +
    "                \"endDate\": \"2018-05-12T10:22:31:000Z\",\n" +
    "                \"id\": \"2\",\n" +
    "                \"serialNumber\": \"0b4a24f9-53eb-421a-a132-e7abb57dbed4\",\n" +
    "                \"transLocations\": [\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:20:00Z[UTC]\",\n" +
    "                        \"distance\": 0,\n" +
    "                        \"lat\": 50.358211,\n" +
    "                        \"lon\": 5.275686,\n" +
    "                        \"price\": 0,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:21:00Z[UTC]\",\n" +
    "                        \"distance\": 0.13189693148428366,\n" +
    "                        \"lat\": 50.359211,\n" +
    "                        \"lon\": 5.274686,\n" +
    "                        \"previousLat\": 50.358211,\n" +
    "                        \"previousLon\": 5.275686,\n" +
    "                        \"price\": 0.006594846574214184,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"countryCode\": \"\",\n" +
    "                        \"dateTime\": \"2018-05-12T10:22:00Z[UTC]\",\n" +
    "                        \"distance\": 0.13189612769850817,\n" +
    "                        \"lat\": 50.360211,\n" +
    "                        \"lon\": 5.273686,\n" +
    "                        \"previousLat\": 50.359211,\n" +
    "                        \"previousLon\": 5.274686,\n" +
    "                        \"price\": 0.006594806384925409,\n" +
    "                        \"serialNumber\": \"1\"\n" +
    "                    }\n" +
    "                ]\n" +
    "            }\n" +
    "        ],\n" +
    "        \"ownerId\": 1,\n" +
    "        \"paymentStatus\": \"OPEN\",\n" +
    "        \"totalAmount\": 0.15\n" +
    "    }\n" +
    "]";
}
