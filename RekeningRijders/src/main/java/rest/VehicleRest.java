package rest;

import domain.Vehicle;
import dto.VehicleDto;
import service.VehicleHistoryService;
import service.VehicleService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

// NOTE: Most actions for vehicles are available in the vehicle-history rest
@Path("/vehicles")
public class VehicleRest {

  @Inject
  private VehicleService vehicleService;

  /**
   * Route to retrieve all vehicles
   *
   * @return                a response containing all vehicles in JSON format
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAll() {
    List<VehicleDto> vehicles = new ArrayList<>();
    int status = 200;

    try {
      for (Vehicle v : vehicleService.findAll()) {
        vehicles.add(VehicleDto.fromVehicle(v));
      }
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicles)
      .build();
  }


  /**
   * Route to retrieve one vehicle by the license plate
   *
   * @param licensePlate    the license plate of the vehicle
   * @return                a response containing a vehicle in JSON format when found
   */
  @GET
  @Path("/by-license-plate/{licensePlate}")
  @Produces({ APPLICATION_JSON })
  public Response getByLicensePlate(@PathParam("licensePlate") String licensePlate) {
    VehicleDto vehicle = null;
    int status = 200;

    try {
      vehicle = VehicleDto.fromVehicle(vehicleService.findByLicensePlate(licensePlate));
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(vehicle)
      .build();
  }

  // TODO
  @GET
  @Path("/by-car-tracker/{carTracker}")
  @Produces({ APPLICATION_JSON })
  public Response getByCarTracker(@PathParam("carTracker") String carTracker) {
    String category = null;
    int status = 200;

    try {
      VehicleDto vehicle = VehicleDto.fromVehicle(vehicleService.findByCarTracker(carTracker));
      category = vehicle.getRateCategory();
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(category)
      .build();
  }
}
