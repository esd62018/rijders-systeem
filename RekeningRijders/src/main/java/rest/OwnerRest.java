package rest;

import dto.OwnerDto;
import service.OwnerService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/owners")
public class OwnerRest {

  @Inject
  private OwnerService ownerService;

  public OwnerRest() {

  }

  /**
   * Route to get all exisiting owners
   *
   * @return      a response containing a list of owners in JSON format on success
   */
  @GET
  @Produces({ APPLICATION_JSON })
  public Response getAllOwners() {
    List<OwnerDto> owners = null;
    int status = 200;

    try {
      owners = ownerService.findAll().stream().map(OwnerDto::fromOwner).collect(Collectors.toList());
    } catch (Exception e) {
      // TODO: handle exceptions explicitly
      status = 500;
    }

    return Response
      .status(status)
      .entity(owners)
      .build();
  }

  /**
   * Route to get one specific owner
   *
   * Required route params:
   *  - id
   *
   * Route params is required and id needs to be existing
   *
   *
   * @param id        the id of the owner
   * @return          a response containing one owner in JSON format on success
   */
  @GET
  @Path("/{id}")
  @Produces({ APPLICATION_JSON })
  public Response getOwner(@PathParam("id") Long id) {
    OwnerDto owner = null;
    int status = 200;

    try {
      owner = OwnerDto.fromOwner(ownerService.findById(id));
    } catch (Exception e) {
      // TODO: handle exceptions explicitly
      status = 500;
    }

    return Response
      .status(status)
      .entity(owner)
      .build();
  }

  /**
   * Route to retrieve an owner by a vehicle's license plate
   *
   * @param licensePlate    the license plate of a vehicle
   * @return                a response containing the owner in JSON format when found
   */
  @GET
  @Path("/by-license-plate/{licensePlate}")
  @Produces({ APPLICATION_JSON })
  public Response getOwnerByLicensePlate(@PathParam("licensePlate") String licensePlate) {
    OwnerDto owner = null;
    int status = 200;

    try {
      owner = OwnerDto.fromOwner(ownerService.findByLicensePlate(licensePlate));
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(owner)
      .build();
  }

  // TODO
  @GET
  @Path("/by-car-tracker/{carTracker}")
  @Produces({ APPLICATION_JSON })
  public Response getOwnerByCarTracker(@PathParam("carTracker") String carTracker) {
    long ownerId = 0;
    int status = 200;

    try {
      OwnerDto owner = OwnerDto.fromOwner(ownerService.findByCarTracker(carTracker));
      ownerId = owner.getId();
    } catch (Exception e) {
      status = 500;
    }

    return Response
      .status(status)
      .entity(ownerId)
      .build();
  }
}
