package dto;

public enum PaymentStatusDto {
  OPEN,
  PAID,
  CANCELLED
}
