package dto;

public class RatePerKilometerDto {
  private Long id;
  private String region;
  private String startDate;
  private String endDate;
  private RateCategory rateCategory;
  private double ratePrice;

  public RatePerKilometerDto() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public RateCategory getRateCategory() {
    return rateCategory;
  }

  public void setRateCategory(RateCategory rateCategory) {
    this.rateCategory = rateCategory;
  }

  public double getRatePrice() { return ratePrice; }

  public void setRatePrice(double ratePrice) {
    this.ratePrice = ratePrice;
  }
}
