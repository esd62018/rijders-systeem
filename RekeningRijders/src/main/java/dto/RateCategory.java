package dto;

public enum RateCategory {
  ELECTRICAL,
  DIESEL,
  GASOLINE
}
