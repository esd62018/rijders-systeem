package dto;

import java.util.List;

public class JourneyDto {
  private String id;
  private String serialNumber;
  private String endDate;
  private List<TransLocationDto> transLocations;

  public JourneyDto() {

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public List<TransLocationDto> getTransLocations() {
    return transLocations;
  }

  public void setTransLocations(List<TransLocationDto> transLocations) {
    this.transLocations = transLocations;
  }
}
