package dto;

import domain.Account;

public class AccountDto {
  private Long id;
  private String username;
  private String password;
  private OwnerDto details;

  public AccountDto () {
    
  }

  public AccountDto(Long id, String username, String password, OwnerDto details) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.details = details;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public OwnerDto getDetails() {
    return details;
  }

  public void setDetails(OwnerDto details) {
    this.details = details;
  }

  public static AccountDto fromAccount(Account account) {
    return new AccountDto(
      account.getId(),
      account.getUsername(),
      account.getPassword(),
      OwnerDto.fromOwner(account.getDetails())
    );
  }
}
