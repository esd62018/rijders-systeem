package dto;

import java.util.List;

public class InvoiceDto {
  private Long id;
  private String carTracker;
  private double totalAmount;
  private String invoiceDate;
  private PaymentStatusDto paymentStatus;
  private long ownerId;
  private List<JourneyDto> journeys;
  private String category;

  public InvoiceDto() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCarTracker() {
    return carTracker;
  }

  public void setCarTracker(String carTracker) {
    this.carTracker = carTracker;
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(String invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public PaymentStatusDto getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(PaymentStatusDto paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(long ownerId) {
    this.ownerId = ownerId;
  }

  public List<JourneyDto> getJourneys() {
    return journeys;
  }

  public void setJourneys(List<JourneyDto> journeys) {
    this.journeys = journeys;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }
}

