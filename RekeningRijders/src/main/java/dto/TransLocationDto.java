package dto;

import java.util.Date;

public class TransLocationDto {
  private Long id;
  private Double lat;
  private Double lon;
  private Double previousLat;
  private Double previousLon;
  private Date dateTime;
  private String serialNumber;
  private String countryCode;
  private double price;
  private double distance;

  public TransLocationDto() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLon() {
    return lon;
  }

  public void setLon(Double lon) {
    this.lon = lon;
  }

  public Double getPreviousLat() {
    return previousLat;
  }

  public void setPreviousLat(Double previousLat) {
    this.previousLat = previousLat;
  }

  public Double getPreviousLon() {
    return previousLon;
  }

  public void setPreviousLon(Double previousLon) {
    this.previousLon = previousLon;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public void setDateTime(Date dateTime) {
    this.dateTime = dateTime;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }
}

