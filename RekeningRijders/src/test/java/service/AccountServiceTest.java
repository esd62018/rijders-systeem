//package service;
//
//import domain.Account;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.EmptyAsset;
//import org.jboss.shrinkwrap.api.spec.JavaArchive;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import javax.ejb.EJBException;
//import javax.inject.Inject;
//
//import static org.junit.Assert.*;
//
//@RunWith(Arquillian.class)
//public class AccountServiceTest {
//  @Deployment
//  public static JavaArchive createDeployment() {
//    return ShrinkWrap.create(JavaArchive.class)
//      .addPackages(true, "dao", "domain", "service")
//      .addAsResource("insert.sql", "META-INF/insert.sql")
//      .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
//      .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//  }
//
//  @Inject
//  private AccountService accountService;
//
//  @Test
//  public void testAuthenticate() {
//    Account user = new Account(1L, "jim", "password");
//    Account auth =  accountService.authenticate(user);
//
//    assertEquals(user.getId(), auth.getId());
//    assertEquals(user.getUsername(), auth.getUsername());
//    assertEquals(user.getPassword(), auth.getPassword());
//  }
//
//  @Test
//  public void testSignUp() {
//    Account newAccount = new Account("new account", "password");
//    Account persisted = accountService.signUp(newAccount);
//
//    assertEquals(newAccount.getUsername(), persisted.getUsername());
//    assertEquals(newAccount.getUsername(), persisted.getUsername());
//  }
//
//  @Test
//  public void testUpdate() {
//    Account jimUpdated = new Account(1L, "jim updated", "password updated");
//    Account persisted = accountService.update(jimUpdated);
//
//    assertEquals(jimUpdated.getUsername(), persisted.getUsername());
//    assertEquals(jimUpdated.getUsername(), persisted.getUsername());
//  }
//
//  @Test(expected = EJBException.class)
//  public void testDelete() {
//    accountService.delete(1L);
//
//    Account auth = new Account(1L, "jim", "password");
//
//    accountService.authenticate(auth);
//    // ^ should fail because account has been deleted and no results can be found anymore
//  }
//}
