//package service;
//
//import domain.Account;
//import domain.Owner;
//import domain.Vehicle;
//import domain.VehicleHistory;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.EmptyAsset;
//import org.jboss.shrinkwrap.api.spec.JavaArchive;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import javax.inject.Inject;
//
//import static org.junit.Assert.*;
//
//@RunWith(Arquillian.class)
//public class VehicleHistoryServiceTest {
//  @Deployment
//  public static JavaArchive createDeployment() {
//    return ShrinkWrap.create(JavaArchive.class)
//      .addPackages(true, "dao", "domain", "service")
//      .addAsResource("insert.sql", "META-INF/insert.sql")
//      .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
//      .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//  }
//
//  @Inject
//  private VehicleHistoryService vehicleHistoryService;
//
//  @Test
//  public void testFindVehicleHistoryById() {
//    assertNotNull(vehicleHistoryService.findVehicleHistoryById(1L));
//  }
//
//  @Test
//  public void testFindVehicleHistoryFromOwner() {
//    int expectedAmountOfCars = 2;
//    int actualAmountOfCars = vehicleHistoryService.findVehicleHistoryFromOwner(1L).size();
//
//    assertEquals(expectedAmountOfCars, actualAmountOfCars);
//  }
//
//  @Test
//  public void testCreate() {
//    Owner owner = new Owner(1L);
//    Vehicle newVehicle = new Vehicle("Opel", "Corsa", "LICENSE PLACE", "1993", "RED");
//
//    VehicleHistory vehicleHistory = new VehicleHistory();
//    vehicleHistory.setOwner(owner);
//    vehicleHistory.setVehicle(newVehicle);
//
//    vehicleHistoryService.create(vehicleHistory);
//
//    int expectedAmountOfHistories = 3;
//    int actualAmountOfHistories = vehicleHistoryService.findVehicleHistoryFromOwner(owner.getId()).size();
//
//    assertEquals(expectedAmountOfHistories, actualAmountOfHistories);
//  }
//
//  @Test
//  public void testUpdate() {
//    VehicleHistory vehicleHistory = new VehicleHistory(1L);
//    vehicleHistory.setOwner(new Owner(1L));
//    vehicleHistory.setVehicle(new Vehicle(1L));
//    vehicleHistory.setFromDate("updated");
//    vehicleHistory.setTillDate("updated");
//
//    VehicleHistory updatedVehicleHistory = vehicleHistoryService.update(vehicleHistory);
//
//    assertEquals("updated", updatedVehicleHistory.getTillDate());
//    assertEquals("updated", updatedVehicleHistory.getFromDate());
//  }
//
//  public void testDelete() {
//    vehicleHistoryService.delete(1L);
//
//    assertNull(vehicleHistoryService.findVehicleHistoryById(1L));
//  }
//}
